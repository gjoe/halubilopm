-- Adminer 3.3.4 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

USE `mentorsdojo`;

DROP TABLE IF EXISTS `member_expertise`;
CREATE TABLE `member_expertise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expertise` varchar(254) NOT NULL,
  `active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `member_expertise` (`id`, `expertise`, `active`) VALUES
(1,	'Business Development',	1),
(2,	'Creative',	1),
(3,	'Finance',	1),
(4,	'Human Resources',	1),
(5,	'Management',	1),
(6,	'Sales and Marketing',	1),
(7,	'Software Development ',	1);

DROP TABLE IF EXISTS `member_profiles`;
CREATE TABLE `member_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mem_id` int(11) NOT NULL,
  `fullname` text NOT NULL,
  `about` text NOT NULL,
  `expertise` text NOT NULL,
  `industries` text NOT NULL,
  `linkedin` text NOT NULL,
  `facebook` text NOT NULL,
  `need_help` text,
  `providing` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `member_profiles` (`id`, `mem_id`, `fullname`, `about`, `expertise`, `industries`, `linkedin`, `facebook`, `need_help`, `providing`) VALUES
(1,	1,	'Jerry Aguirre',	'hahahaha',	'Test',	'test',	'',	'tes',	'test hwelp',	''),
(2,	2,	'Mister Donut',	'hahahaha',	'Test',	'test',	'test',	'tes',	'test',	''),
(3,	3,	'testset',	'awefawe',	'Test',	'wfawef',	'fawefawef',	'fawef',	'',	''),
(4,	4,	'Tee-Rexo',	'I\'m a Tyranosaur',	'Test',	'wfawef',	'fawefawef',	'fawef',	'',	'fweafa'),
(5,	5,	'Someotheruser',	'Im some other user',	'Test',	'',	'',	'',	NULL,	NULL);

DROP TABLE IF EXISTS `member_settings`;
CREATE TABLE `member_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mem_id` int(11) NOT NULL,
  `show_email` int(1) NOT NULL,
  `notify` int(1) NOT NULL,
  `premium` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `member_settings` (`id`, `mem_id`, `show_email`, `notify`, `premium`) VALUES
(1,	2,	0,	1,	0),
(2,	3,	0,	1,	0),
(3,	4,	0,	1,	0),
(4,	5,	0,	1,	0);

DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` text NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(80) NOT NULL,
  `email` text NOT NULL,
  `user_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `members` (`id`, `fullname`, `username`, `password`, `email`, `user_type`) VALUES
(1,	'Jerry Aguirre',	'jaguirre',	'dce4ec3a0d8a30a8a6845380177eb3fb',	'set',	1),
(2,	'Mister Donut',	'mdonut',	'dce4ec3a0d8a30a8a6845380177eb3fb',	'set',	1),
(3,	'testset',	'test',	'dce4ec3a0d8a30a8a6845380177eb3fb',	'set',	1),
(4,	'Mentor Ninja',	'mninja',	'c6b86b6566846e56c9c9ef17b40178dd',	'tesaf',	2),
(5,	'Someotheruser',	'awfawe',	'c6b86b6566846e56c9c9ef17b40178dd',	'tesaf',	2);

DROP TABLE IF EXISTS `tbl_posts`;
CREATE TABLE `tbl_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `content` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_requests`;
CREATE TABLE `tbl_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mem_id` int(11) NOT NULL,
  `mentor_memid` int(11) NOT NULL,
  `message` text NOT NULL,
  `created` datetime NOT NULL,
  `decision` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_topics`;
CREATE TABLE `tbl_topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` text NOT NULL,
  `active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2012-04-28 13:04:18
