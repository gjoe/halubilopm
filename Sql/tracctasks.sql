<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html lang="en" dir="ltr">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="robots" content="noindex">
<title>Export: redwinepm - Adminer</title>
<link rel="stylesheet" type="text/css" href="adminer.php?file=default.css&amp;version=3.3.4">
<script type="text/javascript">
var areYouSure = 'Resend POST data?';
var noResponse = 'No response from server.';
</script>
<script type="text/javascript" src="adminer.php?file=functions.js&amp;version=3.3.4"></script>
<link rel="shortcut icon" type="image/x-icon" href="adminer.php?file=favicon.ico&amp;version=3.3.4" id="favicon">

<body class="ltr nojs" onclick="return bodyClick(event, 'redwinepm&#039;, &#039;');" onkeydown="bodyKeydown(event);" onload="bodyLoad('5.5');">
<script type="text/javascript">
document.body.className = document.body.className.replace(/ nojs/, ' js');
</script>

<div id="loader"><img src="adminer.php?file=loader.gif&amp;version=3.3.4" alt=""></div>
<div id="content">
<p id="breadcrumb"><a href="adminer.php">MySQL</a> &raquo; <a href='adminer.php?username=root' accesskey='1' title='Alt+Shift+1'>Server</a> &raquo; <a href="adminer.php?username=root&amp;db=redwinepm">redwinepm</a> &raquo; Export
<h2>Export: redwinepm</h2>

<form action="" method="post">
<table cellspacing="0">
<tr><th>Output<td><label><input type='radio' name='output' value='text' checked>open</label><label><input type='radio' name='output' value='file'>save</label><label><input type='radio' name='output' value='gz'>gzip</label><label><input type='radio' name='output' value='bz2'>bzip2</label>
<tr><th>Format<td><label><input type='radio' name='format' value='sql' checked>SQL</label><label><input type='radio' name='format' value='csv'>CSV,</label><label><input type='radio' name='format' value='csv;'>CSV;</label><label><input type='radio' name='format' value='tsv'>TSV</label>
<tr><th>Database<td><select name='db_style'><option selected><option>USE<option>DROP+CREATE<option>CREATE<option>CREATE+ALTER</select><label for='checkbox-1'><input type='checkbox' name='routines' value='1' checked id='checkbox-1'>Routines</label><label for='checkbox-2'><input type='checkbox' name='events' value='1' checked id='checkbox-2'>Events</label><tr><th>Tables<td><select name='table_style'><option><option selected>DROP+CREATE<option>CREATE<option>CREATE+ALTER</select><label for='checkbox-3'><input type='checkbox' name='auto_increment' value='1' id='checkbox-3'>Auto Increment</label><label for='checkbox-4'><input type='checkbox' name='triggers' value='1' checked id='checkbox-4'>Triggers</label><tr><th>Data<td><select name='data_style'><option><option>TRUNCATE+INSERT<option selected>INSERT<option>INSERT+UPDATE</select></table>
<p><input type="submit" value="Export">

<table cellspacing="0">
<thead><tr><th style='text-align: left;'><label><input type='checkbox' id='check-tables' onclick='formCheck(this, /^tables\[/);'>Tables</label><th style='text-align: right;'><label>Data<input type='checkbox' id='check-data' onclick='formCheck(this, /^data\[/);'></label></thead>
<tr><td><label for='checkbox-5'><input type='checkbox' name='tables[]' value='tbl_contacts' onclick="formUncheck(&#039;check-tables&#039;);" id='checkbox-5'>tbl_contacts</label><td align='right'><label>0<input type='checkbox' name='data[]' value='tbl_contacts' onclick="formUncheck(&#039;check-data&#039;);" id='checkbox-6'></label>
<tr><td><label for='checkbox-7'><input type='checkbox' name='tables[]' value='tbl_feature_owners' onclick="formUncheck(&#039;check-tables&#039;);" id='checkbox-7'>tbl_feature_owners</label><td align='right'><label>0<input type='checkbox' name='data[]' value='tbl_feature_owners' onclick="formUncheck(&#039;check-data&#039;);" id='checkbox-8'></label>
<tr><td><label for='checkbox-9'><input type='checkbox' name='tables[]' value='tbl_features' onclick="formUncheck(&#039;check-tables&#039;);" id='checkbox-9'>tbl_features</label><td align='right'><label>0<input type='checkbox' name='data[]' value='tbl_features' onclick="formUncheck(&#039;check-data&#039;);" id='checkbox-10'></label>
<tr><td><label for='checkbox-11'><input type='checkbox' name='tables[]' value='tbl_notes' onclick="formUncheck(&#039;check-tables&#039;);" id='checkbox-11'>tbl_notes</label><td align='right'><label>0<input type='checkbox' name='data[]' value='tbl_notes' onclick="formUncheck(&#039;check-data&#039;);" id='checkbox-12'></label>
<tr><td><label for='checkbox-13'><input type='checkbox' name='tables[]' value='tbl_projects' onclick="formUncheck(&#039;check-tables&#039;);" id='checkbox-13'>tbl_projects</label><td align='right'><label>0<input type='checkbox' name='data[]' value='tbl_projects' onclick="formUncheck(&#039;check-data&#039;);" id='checkbox-14'></label>
<tr><td><label for='checkbox-15'><input type='checkbox' name='tables[]' value='tbl_reminders' onclick="formUncheck(&#039;check-tables&#039;);" id='checkbox-15'>tbl_reminders</label><td align='right'><label>0<input type='checkbox' name='data[]' value='tbl_reminders' onclick="formUncheck(&#039;check-data&#039;);" id='checkbox-16'></label>
<tr><td><label for='checkbox-17'><input type='checkbox' name='tables[]' value='tbl_stories' onclick="formUncheck(&#039;check-tables&#039;);" id='checkbox-17'>tbl_stories</label><td align='right'><label>0<input type='checkbox' name='data[]' value='tbl_stories' onclick="formUncheck(&#039;check-data&#039;);" id='checkbox-18'></label>
<tr><td><label for='checkbox-19'><input type='checkbox' name='tables[]' value='tbl_tracc_notes' onclick="formUncheck(&#039;check-tables&#039;);" id='checkbox-19'>tbl_tracc_notes</label><td align='right'><label>0<input type='checkbox' name='data[]' value='tbl_tracc_notes' onclick="formUncheck(&#039;check-data&#039;);" id='checkbox-20'></label>
<tr><td><label for='checkbox-21'><input type='checkbox' name='tables[]' value='tbl_twpm' checked onclick="formUncheck(&#039;check-tables&#039;);" id='checkbox-21'>tbl_twpm</label><td align='right'><label>0<input type='checkbox' name='data[]' value='tbl_twpm' checked onclick="formUncheck(&#039;check-data&#039;);" id='checkbox-22'></label>
<tr><td><label for='checkbox-23'><input type='checkbox' name='tables[]' value='tbl_twpmgroups' onclick="formUncheck(&#039;check-tables&#039;);" id='checkbox-23'>tbl_twpmgroups</label><td align='right'><label>0<input type='checkbox' name='data[]' value='tbl_twpmgroups' onclick="formUncheck(&#039;check-data&#039;);" id='checkbox-24'></label>
<tr><td><label for='checkbox-25'><input type='checkbox' name='tables[]' value='users' onclick="formUncheck(&#039;check-tables&#039;);" id='checkbox-25'>users</label><td align='right'><label>0<input type='checkbox' name='data[]' value='users' onclick="formUncheck(&#039;check-data&#039;);" id='checkbox-26'></label>
</table>
</form>
<p><a href='adminer.php?username=root&amp;db=redwinepm&amp;dump=tbl%25'>tbl</a></div>

<div id="menu">
<h1>
<a href='http://www.adminer.org/' id='h1'>Adminer</a> <span class="version">3.3.4</span>
<a href="http://www.adminer.org/#download" id="version"></a>
</h1>
<form action="" method="post">
<p class="logout">
<a href='adminer.php?username=root&amp;db=redwinepm&amp;sql='>SQL command</a>
<a href='adminer.php?username=root&amp;db=redwinepm&amp;dump=' id='dump' class='active'>Dump</a>
<input type="submit" name="logout" value="Logout" onclick="eventStop(event);">
<input type="hidden" name="token" value="341462">
</p>
</form>
<form action="">
<p>
<input type="hidden" name="username" value="root"><select name='db' onchange="this.form.submit();"><option value="">(database)<option>information_schema<option>cdcol<option>ezclick<option>mentorsdojo<option>mysql<option>performance_schema<option>phpmyadmin<option>pitcha<option selected>redwinepm<option>squawk<option>test<option>webauth</select><input type="submit" value="Use" class='hidden' onclick="eventStop(event);">
<p><a href="adminer.php?username=root&amp;db=redwinepm&amp;create=">Create new table</a>
<p id='tables'>
<a href="adminer.php?username=root&amp;db=redwinepm&amp;select=tbl_contacts">select</a> <a href="adminer.php?username=root&amp;db=redwinepm&amp;table=tbl_contacts" title='Show structure'>tbl_contacts</a><br>
<a href="adminer.php?username=root&amp;db=redwinepm&amp;select=tbl_feature_owners">select</a> <a href="adminer.php?username=root&amp;db=redwinepm&amp;table=tbl_feature_owners" title='Show structure'>tbl_feature_owners</a><br>
<a href="adminer.php?username=root&amp;db=redwinepm&amp;select=tbl_features">select</a> <a href="adminer.php?username=root&amp;db=redwinepm&amp;table=tbl_features" title='Show structure'>tbl_features</a><br>
<a href="adminer.php?username=root&amp;db=redwinepm&amp;select=tbl_notes">select</a> <a href="adminer.php?username=root&amp;db=redwinepm&amp;table=tbl_notes" title='Show structure'>tbl_notes</a><br>
<a href="adminer.php?username=root&amp;db=redwinepm&amp;select=tbl_projects">select</a> <a href="adminer.php?username=root&amp;db=redwinepm&amp;table=tbl_projects" title='Show structure'>tbl_projects</a><br>
<a href="adminer.php?username=root&amp;db=redwinepm&amp;select=tbl_reminders">select</a> <a href="adminer.php?username=root&amp;db=redwinepm&amp;table=tbl_reminders" title='Show structure'>tbl_reminders</a><br>
<a href="adminer.php?username=root&amp;db=redwinepm&amp;select=tbl_stories">select</a> <a href="adminer.php?username=root&amp;db=redwinepm&amp;table=tbl_stories" title='Show structure'>tbl_stories</a><br>
<a href="adminer.php?username=root&amp;db=redwinepm&amp;select=tbl_tracc_notes">select</a> <a href="adminer.php?username=root&amp;db=redwinepm&amp;table=tbl_tracc_notes" title='Show structure'>tbl_tracc_notes</a><br>
<a href="adminer.php?username=root&amp;db=redwinepm&amp;select=tbl_twpm">select</a> <a href="adminer.php?username=root&amp;db=redwinepm&amp;table=tbl_twpm" title='Show structure'>tbl_twpm</a><br>
<a href="adminer.php?username=root&amp;db=redwinepm&amp;select=tbl_twpmgroups">select</a> <a href="adminer.php?username=root&amp;db=redwinepm&amp;table=tbl_twpmgroups" title='Show structure'>tbl_twpmgroups</a><br>
<a href="adminer.php?username=root&amp;db=redwinepm&amp;select=users">select</a> <a href="adminer.php?username=root&amp;db=redwinepm&amp;table=users" title='Show structure'>users</a><br>
<script type='text/javascript'>
var jushLinks = { sql: [ 'adminer.php?username=root&db=redwinepm&table=$&', /\b(tbl_contacts|tbl_feature_owners|tbl_features|tbl_notes|tbl_projects|tbl_reminders|tbl_stories|tbl_tracc_notes|tbl_twpm|tbl_twpmgroups|users)\b/g ] };
jushLinks.bac = jushLinks.sql;
jushLinks.bra = jushLinks.sql;
jushLinks.sqlite_quo = jushLinks.sql;
jushLinks.mssql_bra = jushLinks.sql;
</script>
<input type="hidden" name="dump" value=""></p></form>
</div>
