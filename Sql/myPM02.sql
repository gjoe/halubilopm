CREATE TABLE `t_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `cp_number` varchar(19) NOT NULL,
  `can_create` int(1) NOT NULL,
  `enabled` int(1) NOT NULL,
  `group_id` int(11) NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task` text NOT NULL,
  `assignedto` varchar(100) NOT NULL,
  `assignedby` varchar(100) NOT NULL,
  `estimate` varchar(25) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_twpm_groups` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`groupname` varchar(100) NOT NULL,
PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_stories` (
`id` int(11) NOT NULL AUTO_INCREMENT, 
`story` text NOT NULL,
`stakeholder` varchar(100) NOT NULL,
`tid` int(11) NOT NULL,
`created` datetime NOT NULL,
`modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 
--stakeholder user_id 

CREATE TABLE `tbl_notes` (
`id` INT(11) AUTO_INCREMENT NOT NULL, 
`note` TEXT NOT NULL,
`user_id` INT(11) NOT NULL,
`tid` INT(11) NOT NULL DEFAULT '0',
`sid` INT(11) NOT NULL DEFAULT '0',
`note_type` ENUM('story','task'),
`created` datetime NOT NULL,
`modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `tbl_reminders`(
`id` int(11) NOT NULL AUTO_INCREMENT, 
`user_id` int(11) NOT NULL,
`reminder` text NOT NULL,
`created` datetime NOT NULL,
`deleted` int(1) DEFAULT 0,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- tbl_twpm 
-- id gid url description details created modified
 



