<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $site['title']; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<?php echo $site['description']; ?>">
<meta name="author" content="">

<!-- Le styles -->
<link href="assets/css/bootstrap.mini.css" rel="stylesheet">
<link href="assets/css/custom.css" rel="stylesheet">
<style>
body
{
	padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link href="http://pitchapie.com/css/bootstrap-responsive.css" rel="stylesheet">

<!-- An HTML5 shim-->
<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<link rel="icon" href="images/favicon.ico">
<link rel="shortcut icon" href="assets/img/favicon.ico">
<link rel="apple-touch-icon" href="assets/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

<script src="assets/js/jquery.js"></script> 
<script src="http://pitchapie.com/js/initializer.js"></script>
</head>